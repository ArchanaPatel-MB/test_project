const problem1 = (inventory, value_to_be_serch) => {
    if (typeof inventory != 'undefined') {
        for (let i = 0; i < inventory.length; i++) {
            if (inventory[i].id == value_to_be_serch) {
                return inventory[i];
            }

        }
    }
    return [];
}

module.exports = problem1;
