// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is? Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"

const problem2=require("./problem2");
const inventory=require('./object_Array').inventory;

const result=problem2(inventory);
console.log("Last car is a " +result.car_make +' ' +result.car_model) ;